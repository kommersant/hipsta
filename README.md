# hipsta-server

    Экспериментальный сервер для обкатки технологий и проектов

## Install

Для запуска необходимо установить:

* git (https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* менеджер пакетов yarn (<https://yarnpkg.com/en/docs/install>)
* последнюю версию node.js (<https://nodejs.org/en/>)

Далее необходимо запустить скрипт
```
yarn deploy-and-start
```
