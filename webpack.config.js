'use strict'

const
  webpack = require('webpack'),
  path = require('path'),
  NODE_ENV = process.env.NODE_ENV || 'development',
  entries = require('./webpack.pages.json'),
  ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = [ {
  context: path.resolve(__dirname, 'pages'),
  entry: entries,
  module: {
    loaders: [
      {
        loader: 'babel-loader',
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        query: {
          presets: [ 'stage-1', 'react', 'es2015' ]
        }
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          use: [
            {
              loader: 'css-loader',
              options: { importLoaders: 1 },
            },
            'postcss-loader',
          ],
        }),
      },
    ],
  },
  output: {
    path: path.resolve(__dirname, 'public/compiled/js'),
    filename: '[name].js'
  },
  plugins: [
    new ExtractTextPlugin('../css/[name].css'),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: function (module) {
        return module.context && module.context.indexOf('node_modules') !== -1;
      }
    })
  ]
} ];

if (NODE_ENV == 'production') {
  module.exports.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        drop_console: true,
        unsafe: true
      }
    })
  )
}
