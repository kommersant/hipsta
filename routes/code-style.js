'use strict'

let express = require('express'),
  router = express.Router();

router.get('/', (req, res, next) => {
  res.render('code-style', {
    title: 'Code Styles',
    styles: 'code-style'
  });
});

module.exports = router;
