'use strict'

let express = require('express'),
  router = express.Router(),
  path = require('path');

router.get('/', (req, res, next) => {
  res.sendFile(path.join(__dirname, '../views/index.html'));
});

module.exports = router;
