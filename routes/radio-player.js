'use strict';

var
  express = require('express'),
  route = module.exports = express.Router();

route.get('/', function(req, res) {
  res.render('common/page', {
    title: 'Player',
    styles: '/static/compiled/css/radio-player.css',
    scripts: '/static/compiled/js/radio-player.js',
    vendor: '/static/compiled/js/vendor.js'
  });
});
