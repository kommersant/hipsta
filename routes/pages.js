'use strict'

let express = require('express'),
  router = express.Router(),
  path = require('path'),
  fs = require('fs');

router.get('/', (req, res, next) => {
  res.sendFile(path.join(__dirname, '../views/pages.html'));
});

router.get('/:name', (req, res, next) => {
  let pageFilePath = `../pages/${req.params.name}/index.html`,
    pageFileAbsPath = path.join(__dirname, pageFilePath);

  try {
    fs.statSync(pageFileAbsPath);
  } catch (e) {
    res.send(`Нет файла ${pageFilePath}`);
    return;
  }

  res.sendFile(pageFileAbsPath);
});


module.exports = router;
