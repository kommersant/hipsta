'use strict';

var
  express = require('express'),
  route = module.exports = express.Router();

route.get('/', function(req, res) {
  res.render('common/page', {
    title: 'Timeline',
    styles: '/static/compiled/css/radio-timeline.css',
    scripts: '/static/compiled/js/radio-timeline.js',
    vendor: '/static/compiled/js/vendor.js'
  });
});
