'use strict';

const fs = require('fs'),
  path = require('path'),
  _ = require('lodash');

let routes = (app) => {
  const routesPath = 'routes';

  setRoutes(routesPath);

  function setRoutes(dirName) {
    let
      dirPath = `${__dirname}/../${dirName}`,
      ext = /\.js$/,
      itemsInDir = fs.readdirSync(dirPath),
      directories = itemsInDir.filter((file) => {
        return fs.statSync(path.join(dirPath, file)).isDirectory();
      }),
      files = _.difference(itemsInDir, directories);


    _.forEach(files, (methodName) => {
      let
        isNotJSFile = !ext.test(methodName),
        isPrivate = process.env.NODE_ENV == 'production' && /^_/.test(methodName);

      if (isNotJSFile || isPrivate) return;

      let webPath = dirName.replace(routesPath, '');

      app.use(
        `${webPath}/${methodName.replace('index.js', '').replace(ext, '')}`,
        require(`../${dirName}/${methodName}`)
      );
    });

    _.forEach(directories, (innerDirName) => setRoutes(`${dirName}/${innerDirName}`));
  }
}

module.exports = routes;
