'use strict';

import styles from './player.css';
import React from 'react';
import ReactDOM from 'react-dom';

const Player = React.createClass({
    render() {
      return (
        <div className="player">
          Плеер!
        </div>
      );
    }
  });

module.exports = Player;

ReactDOM.render(<Player />, document.getElementById('content'));
